
# Base SPDs
AkromaSPD = 116
ZairossSPD = 94
GaroSPD = 107

# Leader Skill and Tower Boni
Lead = .30
Tower = .0

def checkSPD(SPD, turns=1000):
	"""
		Checks if specific SPD+ for Garo works with the 67/87 toah Akroma stage

		Parameter:
			SPD : int
				Garos rune speed
			turns : int
				turns Garo should survive (not rounds)
		Returns:
			GaroLives : bool
				True, if Garo is still living
	"""


	Akroma1 = 0
	Akroma2 = 0
	Garo = 30  # boost from Bella at beginning, works also if akroma derp
	Zaiross = 0
	Escape = True
	GaroLives = True
	count = 0
	while count<turns:
		Akroma1 += AkromaSPD*.07
		Akroma2 += AkromaSPD*.07
		Garo += (GaroSPD*(1+Lead+Tower) + SPD)*.07
		# if count<2:
		# 	Zaiross += ZairossSPD*.07

		maxSPD = max([Akroma1, Akroma2, Garo, Zaiross])
		if maxSPD<100: continue
		else: count+=1
		if Akroma1 == maxSPD:
			Akroma1 = 0
			if Escape:
				Garo += 50
				Escape = False
			else:
				GaroLives = False
				break
			continue
		elif Akroma2 == maxSPD:
			Akroma2 = 0
			if Escape:
				Garo += 50
				Escape = False
			else:
				GaroLives = False
				break
			continue
		elif Zaiross == maxSPD:
			Zaiross == 0
			continue
		elif Garo == maxSPD:
			Garo = 0
			Escape = True
			continue
	return GaroLives
	
def getGaroSPDs(minSPD=0, maxSPD=219, turns=1000):
	"""
		Gives all working and not working SPDs in given range

		Paramters:
			minSPD, maxSPD : int, optional
				minimum and maximum rune speed from Garo to check
			turns : int, optional
				turns Garo should survive (not rounds)
		Returns:
			WorkingSPDs, ErrorSPDs : array
				Arrays giving the working SPDs and not working SPDs
	"""

	WorkingSPDs = []
	ErrorSPDs = []
	for SPD in range(minSPD, maxSPD+1):
		if checkSPD(SPD, turns):
			WorkingSPDs.append(SPD)
		else:
			ErrorSPDs.append(SPD)
	return WorkingSPDs, ErrorSPDs		

WorkingSPD, ErrorSPD = getGaroSPDs()
print(f"Lead: {Lead}\nTower: {Tower}\n")
print("SPD that is not working for Garo:\n", ErrorSPD)
print("\nSPD that is working:\n", WorkingSPD)
